package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class UpdateUserRealNameShould {

    private lateinit var action: UpdateUserRealName
    private val repository: UserRepository = mock()
    private lateinit var result: Single<String>

    @Test
    fun `Update user's real name`() {
        givenAnAction()
        givenARepositoryThatReturnsASuccessfulString()
        whenUpdateUserRealName()
        thenAssertUserRealNameIsUpdated()
    }

    @Test
    fun `Throw exception when there is an issue updating the user's real name`() {
        givenAnAction()
        givenARepositoryThatThrowsAnException()
        whenUpdateUserRealName()
        thenThrowUserRepositoryException()
    }

    private fun givenAnAction() {
        action = UpdateUserRealName(repository)
    }


    private fun givenARepositoryThatReturnsASuccessfulString() {
        whenever(repository.updateRealName(updatedUser)).thenReturn(
            Single.just(successCode)
        )
    }

    private fun givenARepositoryThatThrowsAnException() {
        whenever(repository.updateRealName(updatedUser)).thenReturn(
            Single.error(UserRepositoryException())
        )
    }

    private fun whenUpdateUserRealName() {
        result = action(updatedUser.nickName, updatedUser.realName)
    }

    private fun thenAssertUserRealNameIsUpdated() {
        result.test().assertResult(successCode).dispose()
    }

    private fun thenThrowUserRepositoryException() {
        result.test().assertError { it is UserRepositoryException }.dispose()
    }


    private companion object {
        val updatedUser = User("@facu", "Facundo")
        val successCode = "200"
    }
}