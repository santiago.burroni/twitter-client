package com.danverk.twitterclient.config

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.danverk.twitterclient.R
import com.danverk.twitterclient.presentation.user.UserFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, UserFragment.newInstance())
                .commitNow()
        }
    }
}
