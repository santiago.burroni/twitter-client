package com.danverk.twitterclient.infra

import com.danverk.twitterclient.domain.model.user.User
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("registerUser")
    fun registerUser(
        @Query("nickName") nickName: String,
        @Query("realName") realName: String
    ): Single<String>

    @GET("updateUserRealName")
    fun updateUserRealName(
        @Query("nickName") nickName: String,
        @Query("realName") realName: String
    ): Single<String>

    @GET("followUser")
    fun followUser(
        @Query("nickName") nickName: String,
        @Query("toFollow") toFollow: String
    ): Single<String>

    @GET("getFollowedByUser")
    fun getFollowedByUser(
        @Query("nickName") nickName: String
    ): Single<List<User>>
}