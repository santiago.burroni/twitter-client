package com.danverk.twitterclient.domain.model.user

data class FollowIntent(
    val nickName: String,
    val toFollow: String
)