package com.danverk.twitterclient.domain.model.user

import io.reactivex.rxjava3.core.Single

interface UserRepository {
    fun register(user: User): Single<String>
    fun updateRealName(user: User): Single<String>
    fun follow(intent: FollowIntent): Single<String>
    fun getFollowedBy(nickName: String): Single<List<User>>
}