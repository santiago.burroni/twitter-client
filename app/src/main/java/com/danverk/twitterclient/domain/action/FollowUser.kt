package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.FollowIntent
import com.danverk.twitterclient.domain.model.user.UserRepository
import io.reactivex.rxjava3.core.Single

interface FollowUserAction {
    operator fun invoke(nickName: String, toFollow: String): Single<String>
}

class FollowUser(private val repository: UserRepository) : FollowUserAction {

    override operator fun invoke(nickName: String, toFollow: String) =
        repository.follow(FollowIntent(nickName, toFollow))
}