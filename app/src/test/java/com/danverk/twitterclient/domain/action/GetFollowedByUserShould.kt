package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class GetFollowedByUserShould {

    private lateinit var action: GetFollowedByUser
    private val repository: UserRepository = mock()
    private lateinit var result: Single<List<User>>

    @Test
    fun `Return users that are followed by the given user`() {
        givenAnAction()
        givenARepositoryThatReturnsASuccessfulString()
        whenGetFollowedByUser()
        thenAssertUsersFollowedByUserWereReturned()
    }

    @Test
    fun `Throw exception when there is an issue getting the users followed by a given user`() {
        givenAnAction()
        givenARepositoryThatThrowsAnException()
        whenGetFollowedByUser()
        thenThrowUserRepositoryException()
    }

    private fun givenAnAction() {
        action = GetFollowedByUser(repository)
    }


    private fun givenARepositoryThatReturnsASuccessfulString() {
        whenever(repository.getFollowedBy(user.nickName)).thenReturn(
            Single.just(usersFollowed)
        )
    }

    private fun givenARepositoryThatThrowsAnException() {
        whenever(repository.getFollowedBy(user.nickName)).thenReturn(
            Single.error(UserRepositoryException())
        )
    }

    private fun whenGetFollowedByUser() {
        result = action(user.nickName)
    }

    private fun thenAssertUsersFollowedByUserWereReturned() {
        result.test().assertResult(usersFollowed).dispose()
    }

    private fun thenThrowUserRepositoryException() {
        result.test().assertError { it is UserRepositoryException }.dispose()
    }


    private companion object {
        val user = User("@facu", "Facundo")
        val usersFollowed = listOf(
            User("@dvk", "Santiago"),
            User("@flx", "Felipe")
        )
    }
}