package com.danverk.twitterclient.presentation.user

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danverk.twitterclient.domain.action.FollowUserAction
import com.danverk.twitterclient.domain.action.GetFollowedByUserAction
import com.danverk.twitterclient.domain.action.RegisterUserAction
import com.danverk.twitterclient.domain.action.UpdateUserRealNameAction
import com.danverk.twitterclient.domain.model.user.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class UserViewModel(
    private val registerUser: RegisterUserAction,
    private val updateUserRealName: UpdateUserRealNameAction,
    private val followUser: FollowUserAction,
    private val getFollowedByUser: GetFollowedByUserAction
) : ViewModel() {

    sealed class State {
        object ShowLoading : State()
        data class ShowMessage(val message: String) : State()
        data class ShowList(val nickNameList: List<String>) : State()
    }

    val state = MutableLiveData<State>()

    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.dispose()
    }

    fun onRegisterButtonClick(nickName: String, realName: String) {
        showLoading()
        when {
            nickName.isEmpty() -> showMessage(EMPTY_NICK_NAME_MESSAGE)
            realName.isEmpty() -> showMessage(EMPTY_REAL_NAME_MESSAGE)
            else -> launchAction(
                registerUser(nickName, realName),
                { showMessage(formatRegisterUserSuccessMessage(nickName)) },
                { showMessage(REGISTER_USER_FAILURE_MESSAGE) }
            )
        }
    }

    fun onUpdateUserRealNameClick(nickName: String, newRealName: String) {
        showLoading()
        when {
            nickName.isEmpty() -> showMessage(EMPTY_NICK_NAME_MESSAGE)
            newRealName.isEmpty() -> showMessage(EMPTY_NEW_REAL_NAME_MESSAGE)
            else -> launchAction(
                updateUserRealName(nickName, newRealName),
                { showMessage(formatUpdateUserRealNameSuccessMessage(nickName)) },
                { showMessage(UPDATE_USER_REAL_NAME_FAILURE_MESSAGE) }
            )
        }
    }

    fun onFollowUserButtonClick(nickName: String, toFollow: String) {
        showLoading()
        when {
            nickName.isEmpty() -> showMessage(EMPTY_NICK_NAME_MESSAGE)
            toFollow.isEmpty() -> showMessage(EMPTY_TO_FOLLOW_MESSAGE)
            else -> launchAction(
                followUser(nickName, toFollow),
                { showMessage(formatFollowUserSuccessMessage(toFollow)) },
                { showMessage(FOLLOW_USER_FAILURE_MESSAGE) }
            )

        }
    }

    fun onGetFollowedByUserButtonClick(nickName: String) {
        showLoading()
        if (nickName.isEmpty()) showMessage(EMPTY_NICK_NAME_MESSAGE)
        else launchAction(
            getFollowedByUser(nickName),
            { showList(mapToNickName(it)) },
            { showMessage(formatGetFollowedByUserFailureMessage(nickName)) }
        )
    }

    private fun mapToNickName(list: List<User>) = list.map { it.nickName }

    private fun showLoading() {
        state.value = State.ShowLoading
    }

    private fun showMessage(message: String) {
        state.value = State.ShowMessage(message)
    }

    private fun showList(list: List<String>) {
        state.value = State.ShowList(list)
    }

    private fun <T> launchAction(
        actionResult: Single<T>,
        onSuccess: (T) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        compositeDisposable.add(
            actionResult
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onSuccess(it) },
                    { onError(it) })
        )
    }

    private fun formatRegisterUserSuccessMessage(nickName: String) =
        String.format(REGISTER_USER_SUCCESS_MESSAGE_TEMPLATE, nickName)

    private fun formatUpdateUserRealNameSuccessMessage(nickName: String) =
        String.format(UPDATE_USER_REAL_NAME_SUCCESS_MESSAGE_TEMPLATE, nickName)

    private fun formatFollowUserSuccessMessage(toFollow: String) =
        String.format(FOLLOW_USER_SUCCESS_MESSAGE_TEMPLATE, toFollow)

    private fun formatGetFollowedByUserFailureMessage(nickName: String) =
        String.format(GET_FOLLOWED_BY_USER_FAILURE_MESSAGE_TEMPLATE, nickName)

    private companion object {
        const val REGISTER_USER_SUCCESS_MESSAGE_TEMPLATE = "User %s successfully registered"
        const val REGISTER_USER_FAILURE_MESSAGE = "There was an error while registering"
        const val EMPTY_NICK_NAME_MESSAGE = "nickName cannot be empty"
        const val EMPTY_REAL_NAME_MESSAGE = "realName cannot be empty"

        const val UPDATE_USER_REAL_NAME_SUCCESS_MESSAGE_TEMPLATE = "User %s successfully updated"
        const val UPDATE_USER_REAL_NAME_FAILURE_MESSAGE = "There was an error while updating"
        const val EMPTY_NEW_REAL_NAME_MESSAGE = "newRealName cannot be empty"

        const val FOLLOW_USER_SUCCESS_MESSAGE_TEMPLATE = "User %s successfully followed"
        const val FOLLOW_USER_FAILURE_MESSAGE = "There was an error while following"
        const val EMPTY_TO_FOLLOW_MESSAGE = "toFollow cannot be empty"

        const val GET_FOLLOWED_BY_USER_FAILURE_MESSAGE_TEMPLATE =
            "There was an error while retrieving users followed by %s"
    }
}