package com.danverk.twitterclient.presentation.user

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.danverk.twitterclient.RxSchedulersRule
import com.danverk.twitterclient.domain.action.FollowUserAction
import com.danverk.twitterclient.domain.action.GetFollowedByUserAction
import com.danverk.twitterclient.domain.action.RegisterUserAction
import com.danverk.twitterclient.domain.action.UpdateUserRealNameAction
import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.nhaarman.mockito_kotlin.*
import io.reactivex.rxjava3.core.Single
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor

class UserViewModelShould {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val rxSchedulersRule = RxSchedulersRule()

    private val observer: Observer<UserViewModel.State> = mock()
    private val registerUser: RegisterUserAction = mock()
    private val updateUserRealName: UpdateUserRealNameAction = mock()
    private val followUser: FollowUserAction = mock()
    private val getFollowedByUser: GetFollowedByUserAction = mock()

    private lateinit var viewModel: UserViewModel
    private lateinit var capturedStates: ArgumentCaptor<UserViewModel.State>

    @Test
    fun `emit ShowLoading and then ShowMessage success when register button is clicked & nickName and userName are not empty`() {
        givenATestableViewModel()
        givenARegisterUserActionThatReturnsSuccessfully()

        whenRegisterButtonClick()

        thenCaptureShowLoadingAndShowMessage(registerUserSuccessMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage failure when register button is clicked & nickName and userName are not empty and register fails`() {
        givenATestableViewModel()
        givenARegisterUserActionThatReturnsUnsuccessfully()

        whenRegisterButtonClick()

        thenCaptureShowLoadingAndShowMessage(registerUserFailureMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty nickName when register button is clicked & nickName is empty`() {
        givenATestableViewModel()

        whenRegisterButtonClickWithEmptyNickName()

        thenCaptureShowLoadingAndShowMessage(emptyNickNameMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty realName when register button is clicked & realName is empty`() {
        givenATestableViewModel()

        whenRegisterButtonClickWithEmptyRealName()

        thenCaptureShowLoadingAndShowMessage(emptyRealNameMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage success when update user real name button is clicked & nickName and newUserName are not empty`() {
        givenATestableViewModel()
        givenAnUpdateUserRealNameActionThatReturnsSuccessfully()

        whenUpdateUserRealNameClick()

        thenCaptureShowLoadingAndShowMessage(updateUserRealNameSuccessMessage)
    }


    @Test
    fun `emit ShowLoading and then ShowMessage failure when update user real name button is clicked & nickName and newUserName are not empty and update fails`() {
        givenATestableViewModel()
        givenAnUpdateUserRealNameActionThatReturnsUnsuccessfully()

        whenUpdateUserRealNameClick()

        thenCaptureShowLoadingAndShowMessage(updateUserRealNameFailureMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty nickName when update user real name button is clicked & nickName is empty`() {
        givenATestableViewModel()

        whenUpdateUserRealNameClickWithEmptyNickName()

        thenCaptureShowLoadingAndShowMessage(emptyNickNameMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty realName when update user real name button is clicked & newRealName is empty`() {
        givenATestableViewModel()

        whenUpdateUserRealNameClickWithEmptyNewRealName()

        thenCaptureShowLoadingAndShowMessage(emptyNewRealNameMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage success when follow user button is clicked & nickName and toFollow are not empty`() {
        givenATestableViewModel()
        givenAFollowUserActionThatReturnsSuccessfully()

        whenFollowUserButtonClick()

        thenCaptureShowLoadingAndShowMessage(followUserSuccessMessage)
    }


    @Test
    fun `emit ShowLoading and then ShowMessage failure when follow user button is clicked & nickName and toFollow are not empty and follow fails`() {
        givenATestableViewModel()
        givenAFollowUserActionThatReturnsUnsuccessfully()

        whenFollowUserButtonClick()

        thenCaptureShowLoadingAndShowMessage(followUserFailureMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty nickName when follow user button is clicked & nickName is empty`() {
        givenATestableViewModel()
        whenFollowUserButtonClickWithEmptyNickName()

        thenCaptureShowLoadingAndShowMessage(emptyNickNameMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty realName when follow user button is clicked & toFollow is empty`() {
        givenATestableViewModel()
        whenFollowUserButtonClickWithEmptyToFollow()

        thenCaptureShowLoadingAndShowMessage(emptyToFollowMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowList when get followed by user button is clicked & nickName is not empty`() {
        givenATestableViewModel()
        givenAGetFollowedByUserActionThatReturnsAList()

        whenGetFollowedByUserButtonClick()

        thenCaptureTwoEmissions()
        thenFirstValueIsShowLoading()
        thenSecondValueIsShowList()
    }

    @Test
    fun `emit ShowLoading and then ShowMessage failure when get followed by user button is clicked & nickName is not empty and getFollowedByUserFails`() {
        givenATestableViewModel()
        givenAGetFollowedByUserActionThatReturnsUnsuccessfully()

        whenGetFollowedByUserButtonClick()

        thenCaptureShowLoadingAndShowMessage(getFollowedByUserFailureMessage)
    }

    @Test
    fun `emit ShowLoading and then ShowMessage empty nickName when get followed by user button is clicked & nickName is empty`() {
        givenATestableViewModel()

        whenGetFollowedByUserButtonClickWithEmptyNickName()

        thenCaptureShowLoadingAndShowMessage(emptyNickNameMessage)
    }

    private fun givenATestableViewModel() {
        givenAViewModelWithActions()
        givenViewModelIsObserved()
        givenAnArgumentCaptor()
    }

    private fun givenAViewModelWithActions() {
        viewModel = UserViewModel(registerUser, updateUserRealName, followUser, getFollowedByUser)
    }

    private fun givenViewModelIsObserved() {
        viewModel.state.observeForever(observer)

    }

    private fun givenAnArgumentCaptor() {
        capturedStates = ArgumentCaptor.forClass(UserViewModel.State::class.java)
    }

    private fun givenARegisterUserActionThatReturnsSuccessfully() {
        whenever(registerUser(user.nickName, user.realName)).thenReturn(Single.just(successCode))
    }

    private fun givenARegisterUserActionThatReturnsUnsuccessfully() {
        whenever(registerUser(user.nickName, user.realName)).thenReturn(Single.error(exception))
    }

    private fun givenAnUpdateUserRealNameActionThatReturnsSuccessfully() {
        whenever(
            updateUserRealName(user.nickName, newRealName)
        ).thenReturn(Single.just(successCode))
    }

    private fun givenAnUpdateUserRealNameActionThatReturnsUnsuccessfully() {
        whenever(updateUserRealName(user.nickName, newRealName)).thenReturn(Single.error(exception))
    }

    private fun givenAFollowUserActionThatReturnsSuccessfully() {
        whenever(followUser(user.nickName, toFollow)).thenReturn(Single.just(successCode))

    }

    private fun givenAFollowUserActionThatReturnsUnsuccessfully() {
        whenever(followUser(user.nickName, toFollow)).thenReturn(Single.error(exception))
    }

    private fun givenAGetFollowedByUserActionThatReturnsAList() {
        whenever(getFollowedByUser(user.nickName)).thenReturn(Single.just(usersFollowed))
    }

    private fun givenAGetFollowedByUserActionThatReturnsUnsuccessfully() {
        whenever(getFollowedByUser(user.nickName)).thenReturn(Single.error(exception))
    }

    private fun whenRegisterButtonClick() {
        viewModel.onRegisterButtonClick(user.nickName, user.realName)
    }

    private fun whenRegisterButtonClickWithEmptyNickName() {
        viewModel.onRegisterButtonClick("", user.realName)
    }

    private fun whenRegisterButtonClickWithEmptyRealName() {
        viewModel.onRegisterButtonClick(user.nickName, "")
    }

    private fun whenUpdateUserRealNameClick() {
        viewModel.onUpdateUserRealNameClick(user.nickName, newRealName)
    }

    private fun whenUpdateUserRealNameClickWithEmptyNickName() {
        viewModel.onUpdateUserRealNameClick("", newRealName)
    }

    private fun whenUpdateUserRealNameClickWithEmptyNewRealName() {
        viewModel.onUpdateUserRealNameClick(user.nickName, "")
    }

    private fun whenFollowUserButtonClick() {
        viewModel.onFollowUserButtonClick(user.nickName, toFollow)
    }

    private fun whenFollowUserButtonClickWithEmptyNickName() {
        viewModel.onFollowUserButtonClick("", toFollow)

    }

    private fun whenFollowUserButtonClickWithEmptyToFollow() {
        viewModel.onFollowUserButtonClick(user.nickName, "")

    }

    private fun whenGetFollowedByUserButtonClick() {
        viewModel.onGetFollowedByUserButtonClick(user.nickName)
    }

    private fun whenGetFollowedByUserButtonClickWithEmptyNickName() {
        viewModel.onGetFollowedByUserButtonClick("")
    }

    private fun thenCaptureShowLoadingAndShowMessage(message: String) {
        thenCaptureTwoEmissions()
        thenFirstValueIsShowLoading()
        thenSecondValueIsShowMessage(message)
    }

    private fun thenCaptureTwoEmissions() {
        verify(observer, times(2)).onChanged(capture(capturedStates))
    }

    private fun thenFirstValueIsShowLoading() {
        assert(capturedStates.firstValue == UserViewModel.State.ShowLoading)
    }

    private fun thenSecondValueIsShowMessage(message: String) {
        assert(capturedStates.secondValue == UserViewModel.State.ShowMessage(message))
    }

    private fun thenSecondValueIsShowList() {
        assert(
            capturedStates.secondValue == UserViewModel.State.ShowList(
                nickNamesFollowed
            )
        )
    }

    private companion object {
        val user = User("@dvk", "Santiago")
        val newRealName = "Santiago Matias Burroni"

        val toFollow = "@chorch"

        val usersFollowed = listOf(
            user,
            User("@flx", "Felipe")
        )

        val nickNamesFollowed = usersFollowed.map { it.nickName }
        val successCode = "200"
        val exception = UserRepositoryException()

        val registerUserSuccessMessage = "User ${user.nickName} successfully registered"
        val registerUserFailureMessage = "There was an error while registering"
        val emptyNickNameMessage = "nickName cannot be empty"
        val emptyRealNameMessage = "realName cannot be empty"

        val updateUserRealNameSuccessMessage = "User ${user.nickName} successfully updated"
        val updateUserRealNameFailureMessage = "There was an error while updating"
        val emptyNewRealNameMessage = "newRealName cannot be empty"

        val followUserSuccessMessage = "User $toFollow successfully followed"
        val followUserFailureMessage = "There was an error while following"
        val emptyToFollowMessage = "toFollow cannot be empty"

        val getFollowedByUserFailureMessage =
            "There was an error while retrieving users followed by ${user.nickName}"
    }
}