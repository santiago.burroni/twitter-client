package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class RegisterUserShould {

    private lateinit var action: RegisterUser
    private val repository: UserRepository = mock()
    private lateinit var result: Single<String>

    @Test
    fun `Register user`() {
        givenAnAction()
        givenARepositoryThatReturnsASuccessfulString()
        whenRegisterUser()
        thenAssertUserIsRegistered()
    }

    @Test
    fun `Throw exception when there is an issue registering an user`() {
        givenAnAction()
        givenARepositoryThatThrowsAnException()
        whenRegisterUser()
        thenThrowUserRepositoryException()
    }

    private fun givenAnAction() {
        action = RegisterUser(repository)
    }

    private fun givenARepositoryThatReturnsASuccessfulString() {
        whenever(repository.register(user)).thenReturn(
            Single.just(successCode)
        )
    }

    private fun givenARepositoryThatThrowsAnException() {
        whenever(repository.register(user)).thenReturn(
            Single.error(UserRepositoryException())
        )
    }

    private fun whenRegisterUser() {
        result = action(user.nickName, user.realName)
    }

    private fun thenAssertUserIsRegistered() {
        result.test().assertResult(successCode).dispose()
    }

    private fun thenThrowUserRepositoryException() {
        result.test().assertError { it is UserRepositoryException }.dispose()
    }

    private companion object {
        val user = User("@facu", "Facu")
        val successCode = "200"
    }
}