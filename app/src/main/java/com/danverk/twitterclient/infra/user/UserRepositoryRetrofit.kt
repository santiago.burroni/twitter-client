package com.danverk.twitterclient.infra.user

import com.danverk.twitterclient.domain.model.user.FollowIntent
import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.danverk.twitterclient.infra.RetrofitService
import io.reactivex.rxjava3.core.Single

class UserRepositoryRetrofit(private val retrofit: RetrofitService) : UserRepository {

    override fun register(user: User) =
        retrofit.registerUser(user.nickName, user.realName).throwErrorIfCode500()

    override fun updateRealName(user: User) =
        retrofit.updateUserRealName(user.nickName, user.realName).throwErrorIfCode500()

    override fun follow(intent: FollowIntent) =
        retrofit.followUser(intent.nickName, intent.toFollow).throwErrorIfCode500()

    //TODO: this will throw because of GSON if i return "500". get help asap on getting the backend returning codes.
    override fun getFollowedBy(nickName: String) = retrofit.getFollowedByUser(nickName)

    fun Single<String>.throwErrorIfCode500(): Single<String> = this.doOnSuccess {
        if (it == "500") throw UserRepositoryException()
    }
}