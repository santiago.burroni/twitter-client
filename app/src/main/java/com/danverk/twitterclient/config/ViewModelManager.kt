package com.danverk.twitterclient.config

import com.danverk.twitterclient.domain.action.FollowUser
import com.danverk.twitterclient.domain.action.GetFollowedByUser
import com.danverk.twitterclient.domain.action.RegisterUser
import com.danverk.twitterclient.domain.action.UpdateUserRealName
import com.danverk.twitterclient.infra.RetrofitService
import com.danverk.twitterclient.infra.user.UserRepositoryRetrofit
import com.danverk.twitterclient.presentation.user.UserViewModel
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ViewModelManager {

    fun getUserViewModel(): UserViewModel {
        val repository = UserRepositoryRetrofit(
            Retrofit.Builder().client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .baseUrl(ENDPOINT).build()
                .create(RetrofitService::class.java)
        )
        return UserViewModel(
            RegisterUser(repository),
            UpdateUserRealName(repository),
            FollowUser(repository),
            GetFollowedByUser(repository)
        )
    }

    const val ENDPOINT = "http://10.0.2.2:7000/"
}