package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.FollowIntent
import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import com.danverk.twitterclient.domain.model.user.UserRepositoryException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class FollowUserShould {

    private lateinit var action: FollowUser
    private val repository: UserRepository = mock()
    private lateinit var result: Single<String>

    @Test
    fun `Follow the given user`() {
        givenAnAction()
        givenARepositoryThatReturnsASuccessfulString()
        whenFollowUser()
        thenAssertUserIsFollowed()
    }

    @Test
    fun `Throw exception when there is an issue following an user`() {
        givenAnAction()
        givenARepositoryThatThrowsAnException()
        whenFollowUser()
        thenThrowUserRepositoryException()
    }

    private fun givenAnAction() {
        action = FollowUser(repository)
    }


    private fun givenARepositoryThatReturnsASuccessfulString() {
        whenever(repository.follow(FollowIntent(user.nickName, followedUser.nickName))).thenReturn(
            Single.just(successCode)
        )
    }

    private fun givenARepositoryThatThrowsAnException() {
        whenever(repository.follow(FollowIntent(user.nickName, followedUser.nickName))).thenReturn(
            Single.error(UserRepositoryException())
        )
    }

    private fun whenFollowUser() {
        result = action(user.nickName, followedUser.nickName)
    }

    private fun thenAssertUserIsFollowed() {
        result.test().assertResult(successCode).dispose()
    }

    private fun thenThrowUserRepositoryException() {
        result.test().assertError { it is UserRepositoryException }.dispose()
    }

    private companion object {
        val user = User("@facu", "Facundo")
        val followedUser = User("@dvk", "Santiago")
        val successCode = "200"
    }
}