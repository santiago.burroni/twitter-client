package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import io.reactivex.rxjava3.core.Single

interface UpdateUserRealNameAction {
    operator fun invoke(nickName: String, newRealName: String): Single<String>
}

class UpdateUserRealName(private val repository: UserRepository) : UpdateUserRealNameAction {

    override operator fun invoke(nickName: String, newRealName: String) =
        repository.updateRealName(User(nickName, newRealName))
}