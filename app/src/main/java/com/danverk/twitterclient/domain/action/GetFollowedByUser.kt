package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import io.reactivex.rxjava3.core.Single

interface GetFollowedByUserAction {
    operator fun invoke(nickName: String): Single<List<User>>
}

class GetFollowedByUser(private val repository: UserRepository) : GetFollowedByUserAction {

    override operator fun invoke(nickName: String) = repository.getFollowedBy(nickName)
}