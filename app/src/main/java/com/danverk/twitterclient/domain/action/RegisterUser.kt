package com.danverk.twitterclient.domain.action

import com.danverk.twitterclient.domain.model.user.User
import com.danverk.twitterclient.domain.model.user.UserRepository
import io.reactivex.rxjava3.core.Single

interface RegisterUserAction {
    operator fun invoke(nickName: String, realName: String): Single<String>
}

class RegisterUser(private val repository: UserRepository) : RegisterUserAction {

    override operator fun invoke(nickName: String, realName: String) =
        repository.register(User(nickName, realName))
}