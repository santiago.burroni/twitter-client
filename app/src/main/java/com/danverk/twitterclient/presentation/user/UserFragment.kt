package com.danverk.twitterclient.presentation.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.danverk.twitterclient.R
import com.danverk.twitterclient.config.ViewModelManager
import kotlinx.android.synthetic.main.user_fragment.*

class UserFragment : Fragment() {

    companion object {
        fun newInstance() = UserFragment()
    }

    private lateinit var viewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.user_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViewModel()
        setUpButtons()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelManager.getUserViewModel()
        viewModel.state.observe(viewLifecycleOwner, Observer(::onStateChange))
    }

    private fun onStateChange(state: UserViewModel.State) {
        when (state) {
            is UserViewModel.State.ShowLoading -> showLoading()
            is UserViewModel.State.ShowMessage -> showMessage(state.message)
            is UserViewModel.State.ShowList -> showList(state.nickNameList)
        }
    }

    private fun showLoading() {
        loading_bar.visibility = View.VISIBLE
    }

    private fun showMessage(message: String) {
        hideLoading()
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun showList(list: List<String>) {
        hideLoading()
        context?.let {
            get_users_list.adapter = ArrayAdapter(it, android.R.layout.simple_list_item_1, list)
        }
    }

    private fun hideLoading() {
        loading_bar.visibility = View.GONE
    }


    private fun setUpButtons() {
        setUpRegisterUserButton()
        setUpUpdateUserButton()
        setUpFollowUserButton()
        setUpGetFollowedByUserButton()
    }

    private fun setUpRegisterUserButton() {
        register_user_button.setOnClickListener {
            viewModel.onRegisterButtonClick(
                register_user_nick_name_field.text.toString(),
                register_user_real_name_field.text.toString()
            )
        }
    }

    private fun setUpUpdateUserButton() {
        update_user_button.setOnClickListener {
            viewModel.onUpdateUserRealNameClick(
                update_user_nick_name_field.text.toString(),
                update_user_real_name_field.text.toString()
            )
        }
    }

    private fun setUpFollowUserButton() {
        follow_user_button.setOnClickListener {
            viewModel.onFollowUserButtonClick(
                follow_user_nick_name_field.text.toString(),
                follow_user_to_follow_field.text.toString()
            )
        }
    }

    private fun setUpGetFollowedByUserButton() {
        get_followed_button.setOnClickListener {
            viewModel.onGetFollowedByUserButtonClick(
                get_followed_nick_name_field.text.toString()
            )
        }
    }
}